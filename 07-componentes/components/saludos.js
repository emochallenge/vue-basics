Vue.component('saludo1',{
        template: '<h1>Saludo estaticardo</h1>',
        data() {
            return {
                saludo: 'Hola desde Vue'                
            }
        }
    });
        Vue.component('saludo2', {
                template: //html
                `
                <div>
                    <h1>{{saludo}}</h1>
                    <h3>Fijate como entre nuevas instancias del componente no interfieren entre si... cheto no?</h3>
                </div>
                `
                ,
                data() {
                    return {
                        saludo: 'Hola dinamicardo desde Vue'
                    }
                }
            })

