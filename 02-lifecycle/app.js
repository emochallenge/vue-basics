//antes de instanciar la data, llega el beforeCreate
const app = new Vue({
    el: '#app',
    data: {
        saludo: 'Saludos perrícola. Soy ciclo de vida de Vue.'
    },
    beforeCreate() {
        console.log('beforeCreate');
    },
    created() {
        //Al crear los métodos, watchers y events, pero aun no accede al DOM. Aun no se puede acceder a 'el:' (linea 3)
        console.log('created');
    },
    beforeMount() {
        //Se ejecuta antes de insertar el Dom
        console.log('beforeMount');
    },
    mounted() {
        //Se ejecuta al insertar el Dom
        console.log('allá está mounted');
    },
    beforeUpdate() {
        //Al detectar un cambio
        console.log('beforeUpdate');
    },
    updated() {
        //Al realizar los cambios
        console.log('updated');
    },
    beforeDestroy() {
        //Antes de destruir la instancia
        console.log('beforeDestroy');
    },
    destroyed() {
        //Se destruye toda la instancia :(
            console.log('destroyed');
    },

    methods: {
        destruir() {
            this.$destroy();
        }
    }

})