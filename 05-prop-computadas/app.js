const app = new Vue({
    el: '#app',
    data: {
        mensaje: 'alguna gilada',
        contador: 0

    },
    computed: {
        invertido() {
            return this.mensaje.split('').reverse().join('');
        },
        color() {
            return {
                'bg-success': this.contador <= 33,
                'bg-warning': this.contador > 33 && this.contador < 66,
                'bg-danger': this.contador >= 66
            }
        }
    },
});