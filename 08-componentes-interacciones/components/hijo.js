Vue.component('hijo', {
    //fijate en el boton del componente hijo como falla porque no podríamos modificar un padre desde un hijo. USAR VUEX para eso
    template: //html
    `
    <div class="py-5 bg-success">
        <h4>Componente hiji: {{numero}}</h4>
        <h4>Nombre: {{nombre}}</h4>
        <button @click="numero++">+</button>
    </div>
    `,
    props: [
        'numero'
    ],
    data() {
        return {
            nombre: 'Shirley'
        }
    },
    mounted() {
        this.$emit('nombreHiji', this.nombre)
    },
})